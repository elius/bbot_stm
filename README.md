# BBot (Balance Bot)

This is MCU firmware for simple two wheels balance robot. It keep vertical position by IMU sensor.
Main purpose is test different regulation logic: PID, neural net, genetic configuration etc.

Current version use DC brushed motor with gearbox and `BNO055` IMU sensor with embedded fusion logic.
Main CPU is `STM32F401UU`.

For control and configuration UART connection is used. For debug sensor data streaming USB CDC can be used.
It is possible to add ESP8266(ESP32) bridge to the UART for transparent telnet communication.

## IMU sensor

IMU sensor is connected by I2C bus. Before data retrivement sensor must be configured and
then switched to fusion mode from configuration. For new configuration changes sensor must
be switched to configuration mode, reconfigured, and then reenabled.

Also before actual operation sensor must be calibrated. This is done automatically after operation
enabling. For gyro calibration sensor must be holded still for few seconds. For acceleromener - 
sensor must be rotated smoothly over one axis in 6-8 fixed positions with holdings for a few seconds.
For mag calibration sensor must be rotated like `8` figure in the air. You can read calibration status
from register. For future calibration coefficients can be saved from registers. For this operation 
switch sensor to configuration mode, read calibration information and save for future. To restore
switch to configuration mode, restore calibration information and reenable operation.

Internal axis can be remaped to actual bot coordinate system.

For bot operation sensor used in `NDOF` mode.
This is a fusion mode with 9 degrees of freedom where the fused absolute orientation data is
calculated from accelerometer, gyroscope and the magnetometer. The advantages of
combining all three sensors are a fast calculation, resulting in high output data rate,
and high robustness from magnetic field distortions.

Output data rate is `100 Hz` in this mode. 

| Mode | Accel | Mag   | Gyro  | Fusion data |
|------|-------|-------|-------|-------------|
| NDOF | 100Hz | 20Hz  | 100Hz | 100Hz       |

There is output interrupt pin, but in actual firmware if BNO there is no dateReady interrupt.

Therefore, the sensor data should be read cyclically at 100 Hz or less rate.

Euler angles output is in degrees scale (by default) `1 degree = 16 LSB`

## Motor control

## Communication

UART is used for communication from PC. 

