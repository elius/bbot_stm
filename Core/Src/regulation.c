#include "regulation.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <cmsis_os.h>
#include "FreeRTOS.h"

#include "main.h"

#include "bno055.h"

#include "uartio.h"

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

#define LIMIT(x, min, max) ((x<min)?min:((x>max)?max:x))

extern TIM_HandleTypeDef htim2;

#define ANGLE_DATA_SIZE 3
static int16_t anglesData[ANGLE_DATA_SIZE];

char dataPrintBuf[64];

static uint8_t traceOn = 0;
static int enable = 0;
static int kp = 35;
static int ti = 250;
static int td = 0;
static int offset = 0;
static int autoOffset = 0;

static BaseType_t prvRunRegSetTrace(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);

static BaseType_t prvRunRegSetKp(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);
static BaseType_t prvRunRegSetTi(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);
static BaseType_t prvRunRegSetTd(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);
static BaseType_t prvRunRegSetOffset(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvRunRegAutoOffset(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvRunRegEnable(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);

static const CLI_Command_Definition_t xRunRegSetTrace =
{ "rt", "rt <trace level>:\r\n\tSet regulation trace level as 0..9 \r\n",
    prvRunRegSetTrace, 1 };
static const CLI_Command_Definition_t xRunRegSetKp =
{ "rkp", "rkp [kp]:\r\n\tSet PID Kp as int, 10 = 1.0 \r\n", prvRunRegSetKp, -1 };
static const CLI_Command_Definition_t xRunRegSetTi =
{ "rti", "rti [ti]:\r\n\tSet PID Ti as int, 10 = 1.0 sec \r\n", prvRunRegSetTi,
    -1 };
static const CLI_Command_Definition_t xRunRegSetTd =
{ "rtd", "rtd [td]:\r\n\tSet PID Td as int, 10 = 1.0 sec \r\n", prvRunRegSetTd,
    -1 };
static const CLI_Command_Definition_t xRunRegSetOffset =
{ "rso", "rso:\r\n\tSet offset from actual angle \r\n", prvRunRegSetOffset, 0 };
static const CLI_Command_Definition_t xRunRegAutoOffset =
{ "ro", "ro:\r\n\tToggle offset automatic by pwm output\r\n",
    prvRunRegAutoOffset, 0 };
static const CLI_Command_Definition_t xRunRegEnable =
{ "re", "re:\r\n\tToggle regulation enable \r\n", prvRunRegEnable, 0 };

void regulationFunction(void const *argument);
osThreadDef(regulationTask, regulationFunction, osPriorityNormal, 0, 512);

static inline void setPwmEnable(int8_t enable)
{
  HAL_GPIO_WritePin(MotorEnable_GPIO_Port, MotorEnable_Pin, enable);
}

void setPwmOutput(int8_t left, int8_t right)
{
  if (left >= 0)
  {
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 100);
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, 100 - left);
  }
  else
  {
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 100 + left);
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_2, 100);
  };
  if (right >= 0)
  {
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, 100);
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 100 - right);
  }
  else
  {
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, 100 + right);
    __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 100);
  };
}

osThreadId regulationInit()
{
  osThreadId id = osThreadCreate(osThread(regulationTask), NULL);
  FreeRTOS_CLIRegisterCommand(&xRunRegSetTrace);
  FreeRTOS_CLIRegisterCommand(&xRunRegSetKp);
  FreeRTOS_CLIRegisterCommand(&xRunRegSetTi);
  FreeRTOS_CLIRegisterCommand(&xRunRegSetTd);
  FreeRTOS_CLIRegisterCommand(&xRunRegSetOffset);
  FreeRTOS_CLIRegisterCommand(&xRunRegAutoOffset);
  FreeRTOS_CLIRegisterCommand(&xRunRegEnable);
  return id;
}

void regulationFunction(void const *argument)
{
  int integralPart = 0;
  int pwmOut = 0;
  int prevDelta = 0;
  int autoOffsetCouner = 0;
  int autoOffsetPos = 0;
  int autoOffsetNeg = 0;
  for (;;)
  {
    int readCounts = bno055ReadRegisters(BNO055_EULER_H_LSB_ADDR,
    ANGLE_DATA_SIZE * 2, (void*) anglesData);

    if (readCounts == ANGLE_DATA_SIZE * 2)
    {
      int angleDelta = anglesData[1] - offset;
      int enableMotors = (abs(angleDelta) < DEG2HW(45)) && enable;
      setPwmEnable(enableMotors);
      if (enableMotors)
      {
        if ((pwmOut < 100) && (pwmOut > -100))
          integralPart += angleDelta;
        prevDelta = angleDelta;
        if (autoOffset)
        {
          if (++autoOffsetCouner >= 20)
          {
            autoOffsetCouner = 0;
            if (abs(angleDelta) < 32)
            {
              if (autoOffsetPos > autoOffsetNeg)
                offset -= 2;
              if (autoOffsetPos < autoOffsetNeg)
                offset += 2;
            }
            autoOffsetPos = 0;
            autoOffsetNeg = 0;
          }
          else
          {
            if (pwmOut > 0)
              autoOffsetPos++;
            if (pwmOut < 0)
              autoOffsetNeg++;
          }
        }
      }
      else // enableMotors
      {
        integralPart = 0;
        prevDelta = 0;
        autoOffsetPos = 0;
        autoOffsetNeg = 0;
        autoOffsetCouner = 0;
      }
      int pidOut = angleDelta * kp / 10 + integralPart * ti / 1000
          + (angleDelta - prevDelta) * td / 100;
      pwmOut = HW2DEG(pidOut);

      pwmOut = LIMIT(pwmOut, -100, 100);
      setPwmOutput(pwmOut, pwmOut);

      if (traceOn)
      {
        char *str = dataPrintBuf;
        *(str++) = '$';
        *(str++) = ' ';
        str += printAngle(str, offset);
        *(str++) = ',';
        str += printAngle(str, angleDelta);
        *(str++) = ',';
        str += sprintf(str, "%+03d", pwmOut);
        *(str++) = '\r';
        *(str++) = '\n';
        serialQueueData(dataPrintBuf, str - dataPrintBuf);
      }
    }
    else
      vTaskDelay(100);
    vTaskDelay(10);
  }
}

static BaseType_t prvRunRegSetTrace(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  unsigned int level = 0;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  if (sscanf(param, "%u", &level) == 1)
  {
    if (level > 9)
      level = 9;
    traceOn = level;
    *pcWriteBuffer = '\0';
  }
  else
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Incorrect trace level!\r\n");
  }
  return pdFALSE;
}

static BaseType_t prvRunRegEnable(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  enable = LIMIT(enable, 0, 1);
  enable = 1 - enable;
  *pcWriteBuffer = '\0';
  snprintf(pcWriteBuffer, xWriteBufferLen, "Enable = %d\r\n", enable);
  return pdFALSE;
}

static BaseType_t prvRunRegSetKp(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  int value = 0;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  if (param != NULL)
  {
    if (sscanf(param, "%d", &value) == 1)
      kp = value;
    *pcWriteBuffer = '\0';
  }
  else
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Kp = %d\r\n", kp);
  }
  return pdFALSE;
}

static BaseType_t prvRunRegSetTi(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  int value = 0;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  if (param != NULL)
  {
    if (sscanf(param, "%d", &value) == 1)
      ti = value;
    *pcWriteBuffer = '\0';
  }
  else
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Ti = %d\r\n", ti);
  }
  return pdFALSE;
}

static BaseType_t prvRunRegSetTd(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  int value = 0;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  if (param != NULL)
  {
    if (sscanf(param, "%d", &value) == 1)
      td = value;
    *pcWriteBuffer = '\0';
  }
  else
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Td = %d\r\n", td);
  }
  return pdFALSE;
}

static BaseType_t prvRunRegSetOffset(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString)
{
  if (autoOffset == 0)
    offset = anglesData[1];
  char strAngle[8] = "";
  printAngle(strAngle, offset);
  strAngle[7] = '\0';
  snprintf(pcWriteBuffer, xWriteBufferLen, "Offset = %s\r\n", strAngle);
  return pdFALSE;
}

static BaseType_t prvRunRegAutoOffset(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString)
{
  autoOffset = LIMIT(autoOffset, 0, 1);
  autoOffset = 1 - autoOffset;
  snprintf(pcWriteBuffer, xWriteBufferLen, "Auto offset = %d\r\n", autoOffset);
  return pdFALSE;
}
