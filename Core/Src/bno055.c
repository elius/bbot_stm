/*
 * bno050.c
 *
 *  Created on: Jan 11, 2021
 *      Author: arhi
 */

#include <bno055.h>

#include <string.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

#include "main.h"

#define BNO055_ADDRESS 0x29 << 1

// I2C DMA buffer
#define I2C_BUF_SIZE 8
static uint8_t i2cBuffer[I2C_BUF_SIZE];

/// \brief Pointer to HAL handler of I2C.
I2C_HandleTypeDef *hBnoI2C = 0;

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
  //HAL_GPIO_WritePin(USR_LED_GPIO_Port, USR_LED_Pin, GPIO_PIN_SET);
  //i2cReadInProgress = 0;
}

static BaseType_t prvRunBNOReadRegister(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString);

static BaseType_t prvRunBNOWriteRegister(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString);

static BaseType_t prvRunBNOReadSensor(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString);

static const CLI_Command_Definition_t xRunBNOReadRegister =
{ "br", "br <register>:\r\n\tRead single register\r\n",
    prvRunBNOReadRegister, 1 };

static const CLI_Command_Definition_t xRunBNOWriteRegister =
{ "bw", "bw <register> <value>:\r\n\tWrite single register\r\n",
    prvRunBNOWriteRegister, 2 };

static const CLI_Command_Definition_t xRunBNOReadSensor =
{ "brs", "brs <sensor code>:\r\n\tRead sensor data: a/m/g/e/l/G - acc/mag/gyr/eul/lin/Grv\r\n",
    prvRunBNOReadSensor, 1 };

int bno055Init(I2C_HandleTypeDef *hi2c)
{
  hBnoI2C = hi2c;
  HAL_StatusTypeDef writeStatus;
  uint8_t data;
  data = 0;
  writeStatus = HAL_I2C_Mem_Write(hBnoI2C, BNO055_ADDRESS, BNO055_PAGE_ID_ADDR,
  I2C_MEMADD_SIZE_8BIT, &data, 1, 1);
  if (writeStatus != HAL_OK)
    return -1;
  data = OPERATION_MODE_CONFIG;
  writeStatus = HAL_I2C_Mem_Write(hBnoI2C, BNO055_ADDRESS, BNO055_OPR_MODE_ADDR,
  I2C_MEMADD_SIZE_8BIT, &data, 1, 1);
  if (writeStatus != HAL_OK)
    return -1;
  data = OPERATION_MODE_NDOF;
  writeStatus = HAL_I2C_Mem_Write(hBnoI2C, BNO055_ADDRESS, BNO055_OPR_MODE_ADDR,
  I2C_MEMADD_SIZE_8BIT, &data, 1, 1);
  if (writeStatus != HAL_OK)
    return -1;
  FreeRTOS_CLIRegisterCommand(&xRunBNOReadRegister);
  FreeRTOS_CLIRegisterCommand(&xRunBNOWriteRegister);
  FreeRTOS_CLIRegisterCommand(&xRunBNOReadSensor);
  return 0;
}

uint8_t bno055ReadRegister(uint8_t address)
{
  HAL_StatusTypeDef readStatus = HAL_I2C_Mem_Read(hBnoI2C, BNO055_ADDRESS,
      address, I2C_MEMADD_SIZE_8BIT, i2cBuffer, 1, 100);
  if (readStatus != HAL_OK)
    return 255;
  return i2cBuffer[0];
}

int8_t bno055ReadRegisters(uint8_t address, uint8_t count, void* data)
{
  HAL_StatusTypeDef readStatus = HAL_I2C_Mem_Read(hBnoI2C, BNO055_ADDRESS,
      address, I2C_MEMADD_SIZE_8BIT, (uint8_t*)data, count, 100);
  if (readStatus != HAL_OK)
    return -1;
  return count;
}


int8_t bno055WriteRegister(uint8_t address, uint8_t value)
{
  i2cBuffer[0] = value;
  HAL_StatusTypeDef writeStatus = HAL_I2C_Mem_Write(hBnoI2C, BNO055_ADDRESS,
      address, I2C_MEMADD_SIZE_8BIT, i2cBuffer, 1, 100);
  if (writeStatus != HAL_OK)
    return -1;
  return 0;
}

int printAngle(char *str, int16_t angle)
{
  if (angle < 0)
  {
    *(str++) = '-';
    angle = -angle;
  }
  else
    *(str++) = '+';
  int16_t angleCeil = angle >> 4;
  int16_t anglePart = angle & 0x000f;
  str += sprintf(str, "%03d.%02d", angleCeil, (anglePart * 100) >> 4);
  return 7;
}

static BaseType_t prvRunBNOReadRegister(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString)
{
  uint16_t address = 255;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  sscanf(param, "%hx", &address);
  if (address < 255)
  {
    uint8_t value = bno055ReadRegister(address);
    snprintf(pcWriteBuffer, xWriteBufferLen, "%02x\r\n", value);
  }
  else
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Incorrect register address!\r\n");
  }
  return pdFALSE;
}

static BaseType_t prvRunBNOWriteRegister(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString)
{
  uint16_t address = 0xffff;
  uint16_t value = 0xffff;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  sscanf(param, "%hx", &address);
  if (address > 255)
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Incorrect register address!\r\n");
    return pdFALSE;
  }
  param = FreeRTOS_CLIGetParameter(pcCommandString, 2, &paramLen);
  sscanf(param, "%hx", &value);
  if (value > 255)
  {
    snprintf(pcWriteBuffer, xWriteBufferLen, "Incorrect value!\r\n");
    return pdFALSE;
  }
  int8_t result = bno055WriteRegister(address, value);
  if (result < 0)
    snprintf(pcWriteBuffer, xWriteBufferLen, "Error!\r\n");
  else
    snprintf(pcWriteBuffer, xWriteBufferLen, "Ok\r\n");
  return pdFALSE;
}

static BaseType_t prvRunBNOReadSensor(char *pcWriteBuffer,
    size_t xWriteBufferLen, const char *pcCommandString)
{
  char sensor = 0;
  BaseType_t paramLen;
  const char *param = FreeRTOS_CLIGetParameter(pcCommandString, 1, &paramLen);
  sscanf(param, "%c", &sensor);
  uint8_t sensorAddress = 0;
  int16_t values[3];
  switch (sensor)
  {
  case 'a':
    sensorAddress = BNO055_ACCEL_DATA_X_LSB_ADDR;
    break;
  case 'm':
    sensorAddress = BNO055_MAG_DATA_X_LSB_ADDR;
    break;
  case 'g':
    sensorAddress = BNO055_GYRO_DATA_X_LSB_ADDR;
    break;
  case 'e':
    sensorAddress = BNO055_EULER_H_LSB_ADDR;
    break;
  case 'l':
    sensorAddress = BNO055_LINEAR_ACCEL_DATA_X_LSB_ADDR;
    break;
  case 'G':
    sensorAddress = BNO055_GRAVITY_DATA_X_LSB_ADDR;
    break;
  default:
    snprintf(pcWriteBuffer, xWriteBufferLen, "Incorrect sensor code!\r\n");
    return pdFALSE;
  }
  if (bno055ReadRegisters(sensorAddress, 3*2, (void*)values) > 0)
    snprintf(pcWriteBuffer, xWriteBufferLen, "%d %d %d\r\n", values[0], values[1], values[2]);
  else
    snprintf(pcWriteBuffer, xWriteBufferLen, "Error\r\n");
  return pdFALSE;
}
