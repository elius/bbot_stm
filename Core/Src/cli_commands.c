/**
 * \file cli_commands.c
 * \date 10-January-2017
 * \author arhi
 * \brief This file contains functions for CLI.
 */

/** @addtogroup CLI
 * @{
 */

#include "cli_commands.h"
#include <string.h>
#include <stdio.h>
#include "task.h"

#define BYTEOF(i, b) (uint8_t)((i >> (8*b)) & 0xFFu)

//extern struct netif gnetif;

/**
 * \brief \c hello command execution function
 * \param[in,out] pcWriteBuffer Pointer to output buffer
 * \param[in] xWriteBufferLen Length of \p pcWriteBuffer
 * \param[in] pcCommandString Pointer to command arguments buffer
 * \return pdFALSE if command fully executed, pdTRUE if need more call
 */
static BaseType_t prvRunHello(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString);

/// \c hello command definition structure.
static const CLI_Command_Definition_t xRunHello =
{ "hello", "hello:\r\n\tDemo hello function\r\n\r\n", prvRunHello, 0 };

void vRegisterCLICommands(void)
{
  // Register all the command line commands defined immediately above.
  FreeRTOS_CLIRegisterCommand(&xRunHello);
}

static BaseType_t prvRunHello(char *pcWriteBuffer, size_t xWriteBufferLen,
    const char *pcCommandString)
{
  snprintf(pcWriteBuffer, xWriteBufferLen, "Hello :)\r\n");
  return pdFALSE;
}

/**
 * @}
 */
