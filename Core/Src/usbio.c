#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

#ifdef __GNUC__
#include <sys/unistd.h>
#endif

#include "usbd_cdc_if.h"

#ifndef __GNUC__
#define EINVAL 22
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
#define EBADF 9
#endif

int _write(int file, char *ptr, int len)
{
    uint8_t ret = USBD_OK;
    switch (file)
    {
    case STDOUT_FILENO: /*stdout*/
        ret = CDC_Transmit_FS((uint8_t*)ptr, len);
        break;
    case STDERR_FILENO: /* stderr */
        ret = CDC_Transmit_FS((uint8_t*)ptr, len);
        break;
    default:
        errno = EBADF;
        return -1;
    }
    if (ret == USBD_OK)
        return len;
    if (ret == USBD_BUSY)
        return 0;
    errno = EBADF;
    return -1;
}
