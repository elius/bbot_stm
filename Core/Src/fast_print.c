/*
 * fast_print.c
 *
 *  Created on: May 12, 2017
 *      Author: arhi
 */

#include "fastPrint.h"

/**
 *  Print LS digit of 8-bit integer
 */
char* fastPrint01u(char* str, uint8_t val)
{
    *(str++) = '0' + val % 10;
    return str;
}

/**
 *  Print 16-bit integer as unsigned 04 formatted string
 */
char* fastPrint04u(char* str, uint16_t val)
{
    str += 3;
    for (int i = 0; i < 4; i++)
    {
        *(str--) = '0' + val % 10;
        val /= 10;
    }
    return str + 5;
}

/**
 *  Print 16-bit integer as signed 05 formatted string
 */
char* fastPrint06s(char* str, int16_t val)
{
    int32_t value;
    if (val >= 0) {
        *(str++) = '+';
    	value = (int32_t)val;
    }
    else
    {
        *(str++) = '-';
        value = (int32_t)(-val);
    }
    str += 4;
    for (int i = 0; i < 5; i++)
    {
        *(str--) = '0' + value % 10;
        value /= 10;
    }
    return str + 6;
}

char* fastPrint02x(char* str, uint8_t val)
{
  for (int8_t p = 4; p >= 0; p -= 4)
  {
    uint8_t place = (val >> p) & 0x0f;
    if (place > 9)
      *(str++) = 'a' + place - 10;
    else
      *(str++) = '0' + place;
  }
  return str;
}
