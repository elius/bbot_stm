/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USR_LED_Pin GPIO_PIN_13
#define USR_LED_GPIO_Port GPIOC
#define MotorL1_Pin GPIO_PIN_0
#define MotorL1_GPIO_Port GPIOA
#define MotorL2_Pin GPIO_PIN_1
#define MotorL2_GPIO_Port GPIOA
#define MotorR1_Pin GPIO_PIN_2
#define MotorR1_GPIO_Port GPIOA
#define MotorR2_Pin GPIO_PIN_3
#define MotorR2_GPIO_Port GPIOA
#define MotorEnable_Pin GPIO_PIN_4
#define MotorEnable_GPIO_Port GPIOA
#define BNO_INT_Pin GPIO_PIN_5
#define BNO_INT_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define USE_SERIAL_CLI
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
