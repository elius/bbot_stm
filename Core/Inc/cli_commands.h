/**
 * \file cli_commands.c
 * \date 10-January-2017
 * \author arhi
 */

/** @addtogroup CLI
 * @{
 */

#ifndef CLI_COMMANDS_H_
#define CLI_COMMANDS_H_

#include "FreeRTOS.h"
#include "FreeRTOS_CLI.h"

/// \brief Clear screen escape sequence
#define CLI_CLS "\x1b[2J\x1b[;H"
/// \brief Greeting escape sequence
#define CLI_GREETING "\x1b[32m>\x1b[m"
/// \brief EOL escape sequence
#define CLI_EOL "\r\n"

/**
 * \brief Register all commands in FreeRTOS CLI core.
 */
extern void vRegisterCLICommands(void);

#endif /* CLI_COMMANDS_H_ */

/**
 * @}
 */
