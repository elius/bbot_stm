/*
 * fastPrint.h
 *
 *  Created on: Apr 24, 2018
 *      Author: arhi
 */

#ifndef FASTPRINT_H_
#define FASTPRINT_H_

#include <stdint.h>

/**
 *  Print LS digit of 8-bit integer
 */
char* fastPrint01u(char* str, uint8_t val);

/**
 *  Print 16-bit integer as unsigned 04 formatted string
 */
char* fastPrint04u(char* str, uint16_t val);

/**
 *  Print 16-bit integer as signed 05 formatted string
 */
char* fastPrint06s(char* str, int16_t val);

#endif /* FASTPRINT_H_ */
